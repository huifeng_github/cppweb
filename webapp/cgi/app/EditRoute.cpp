#include <webx/menu.h>
#include <dbentity/T_XG_ROUTE.h>

class EditRoute : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(EditRoute)

int EditRoute::process()
{
	param_string(id);
	param_string(name);
	param_string(flag);
	param_string(host);
	param_string(port);
	param_string(remark);
	param_string(enabled);

	webx::CheckFileName(id, 0);
	webx::CheckFileName(name, 0);

	checkLogin();
	checkSystemRight();

	int res = 0;
	CT_XG_ROUTE tab;
	sp<DBConnect> dbconn = webx::GetDBConnect();

	tab.init(dbconn);

	tab.user = user;
	tab.remark = remark;

	if (id.length() > 0) tab.id = id;
	if (name.length() > 0) tab.name = name;
	if (host.length() > 0) tab.host = host;
	if (port.length() > 0) tab.port = port;
	if (enabled.length() > 0) tab.enabled = enabled;

	tab.statetime.update();

	if (flag == "D")
	{
		if (id.empty()) return simpleResponse(XG_PARAMERR);

		res = tab.remove();
	}
	else if (flag == "U")
	{
		if (id.empty()) return simpleResponse(XG_PARAMERR);

		res = tab.update();
	}
	else if (flag == "A")
	{
		if (name.empty() || host.empty() || port.empty() || enabled.empty()) return simpleResponse(XG_PARAMERR);

		for (int i = 0; i < 5; i++)
		{
			tab.id = DateTime::GetBizId();

			if ((res = tab.insert()) >= 0) break;
		}
	}
	else
	{
		res = XG_PARAMERR;
	}

	if (res >= 0)
	{
		session = webx::GetLocaleSession("SYSTEM_ROUTELIST");
		
		if (session) session->clear();

		json["statetime"] = tab.statetime.toString();
	}

	json["code"] = res;
	out << json;
	
	return XG_OK;
}