#include <webx/menu.h>
#include <webx/route.h>
#include <dbentity/T_XG_PARAM.h>

class EditParam : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(EditParam)

int EditParam::process()
{
	param_string(id);
	param_string(flag);
	param_string(name);
	param_string(param);
	param_string(filter);
	param_string(remark);

	webx::CheckFileName(id, 0);
	webx::CheckFileName(name, 0);
	webx::CheckSystemRight(this);

	int res = 0;
	CT_XG_PARAM tab;

	tab.init(webx::GetDBConnect());

	tab.id = id;
	tab.statetime.update();
	if (name.length() > 0) tab.name = name;
	if (param.length() > 0) tab.param = param;
	if (filter.length() > 0) tab.filter = filter;
	if (remark.length() > 0) tab.remark = remark;

	if (flag == "U")
	{
		res = tab.update();
	}
	else if (flag == "D")
	{
		res = tab.remove();
	}
	else if (flag == "A")
	{
		if (param.empty() && filter.length() > 2)
		{
			vector<string> vec;

			filter = stdx::trim(filter, "()");

			if (stdx::split(vec, filter, ",") > 0) tab.param = stdx::trim(vec[0]);
		}
		
		if (tab.find() && tab.next()) return simpleResponse(XG_DUPLICATE);

		res = tab.insert();
	}
	else
	{
		res = XG_PARAMERR;
	}

	if (res > 0)
	{
		session = webx::GetLocaleSession("SYSTEM_PARAMETER");

		if (session) session->remove(id);
	}

	json["code"] = res;
	out << json;

	return XG_OK;
}