#!/usr/bin/python

import os;
import sys;

sys.path.append(os.environ['SOURCE_HOME'] + '/library/python');
sys.path.append(os.environ['SOURCE_HOME'] + '/webapp');

import dbx;
import webx;
import stdx;

from pyc import __load__ as app;

app.setup(None);

request = webx.HttpRequest();
request.setParameter('user', 'xungen');
request.setParameter('passwd', 'xungen');

data = request.toString();

stdx.puts('--- request message -----------------------------------------------------------');
stdx.puts('-------------------------------------------------------------------------------');
stdx.puts(data[1]);
stdx.puts('\n');

res = app.process('/testdemo', data[0], data[1]);

if isinstance(res, list) or isinstance(res, tuple):
	stdx.puts('--- response message ----------------------------------------------------------');
	stdx.puts('-------------------------------------------------------------------------------');
	stdx.puts(res[0]);
	if len(res[1]) > 0:
		stdx.puts('\n');
		stdx.puts('--- head message --------------------------------------------------------------');
		stdx.puts('-------------------------------------------------------------------------------');
		stdx.puts(res[1]);
	if len(res[2]) > 0:
		stdx.puts('\n');
		stdx.puts('--- log message ---------------------------------------------------------------');
		stdx.puts('-------------------------------------------------------------------------------');
		stdx.puts(res[2]);
	stdx.puts('\n');
else:
	stdx.puts('--- response message ----------------------------------------------------------');
	stdx.puts('-------------------------------------------------------------------------------');
	stdx.puts(res);

