import imp;

def update(py):
	return imp.reload(py);
	
def getcgimap():
	cgimap = {};

	from pyc import testdemo, textlogo, checkcode;
	cgimap['testdemo'] = lambda app: update(testdemo).main(app);
	cgimap['textlogo'] = lambda app: update(textlogo).main(app);
	cgimap['checkcode'] = lambda app: update(checkcode).main(app);

	return cgimap;