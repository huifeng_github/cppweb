#define XG_ENTITY_EXPORT_INC_PATH	""
#define XG_ENTITY_EXPORT_SRC_PATH	"src"

#include <stdx/all.h>
#include <dbx/DBConnectPool.h>
#include "EntityCreator.h"

bool GetDBConnectPool(DBConnectPool*& dbconnpool, const ConfigFile& file)
{
	string dbdllpath;
	static DllFile dbconnpooldll;
	CREATE_DBCONNECTPOOL_FUNC crtfunc;

	CHECK_FALSE_RETURN(file.getVariable("DLLPATH", dbdllpath));
	CHECK_FALSE_RETURN(dbconnpooldll.open(dbdllpath) && dbconnpooldll.read(crtfunc, "CreateDBConnectPool"));
	CHECK_FALSE_RETURN((dbconnpool = crtfunc()) && dbconnpool->init(file));
	
	return true;
}

int main(int argc, char* argv[])
{
	Process::Instance(argc, argv);

	string nmregex;
	string srcpath;
	ConfigFile file;
	sp<DBConnect> conn;
	DBConnectConfig addr;
	DBConnectPool* dbconnpool;
	
	if (argc < 2)
	{
		puts("please input configure filename");

		goto __EXIT__;
	}

	if (!file.open(argv[1]) || !EntityCreator::LoadConfig(file, addr, nmregex, srcpath))
	{
		puts("load configure failed");

		goto __EXIT__;
	}

	if (GetDBConnectPool(dbconnpool, file) && (conn = dbconnpool->get()))
	{
		printf("DBTYPE : %s\n", conn->getSystemName());
		printf("DBNAME : %s\n", addr.name.c_str());

		puts("---------------------------------------------");

		EntityCreator::ExportEntity(conn.get(), nmregex, srcpath);
	}
	else
	{
		puts("connect the database failed");

		goto __EXIT__;
	}
	
	puts("---------------------------------------------");

__EXIT__:

	return 0;
}
