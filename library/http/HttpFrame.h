#ifndef XG_HTTPFRAME_H
#define XG_HTTPFRAME_H
//////////////////////////////////////////////////////////
#include "HttpCoder.h"


class HttpFrame : public Object
{
	friend class HttpStream;
	friend class HttpRequest;

private:
	u_int32 id;
	u_int32 rid;
	HttpHeadVector& vec;
	mutable u_char type;
	mutable u_char flag;
	mutable u_char weight;
	mutable u_int32 hdrsz;
	mutable u_int32 datsz;
	mutable u_int32 maxsz;
	mutable u_int32 maxcnt;

	mutable string path;
	mutable string param;
	mutable SmartBuffer data;
	mutable HttpHeadNode head;

public:
	int decode() const;
	int getMethod() const;
	int init(const void* msg, int len);
	int add(const HttpFrame& obj) const;

public:
	static SmartBuffer Encode(u_int32 id, HttpHeadNode& head, SmartBuffer data, HttpHeadVector& vec);

public:
	HttpFrame(HttpHeadVector& tab) : vec(tab)
	{
		this->id = 0;
		this->rid = 0;
		this->type = 0;
		this->flag = 0;
		this->hdrsz = 0;
		this->datsz = 0;
		this->maxsz = 0;
		this->maxcnt = 0;
	}
	bool operator < (const HttpFrame& obj) const
	{
		return id < obj.id;
	}
	bool operator == (const HttpFrame& obj) const
	{
		return id == obj.id;
	}

public:
	int size() const
	{
		return data.size();
	}
	u_int32 getId() const
	{
		return id;
	}
	u_char getType() const
	{
		return type;
	}
	u_char getFlag() const
	{
		return flag;
	}
	bool isAddFrame() const
	{
		return type == HTTP2_CONTINUATION_FRAME;
	}
	bool isRstFrame() const
	{
		return type == HTTP2_RSTSTREAM_FRAME;
	}
	bool isDataFrame() const
	{
		return type == HTTP2_DATA_FRAME;
	}
	bool isHeadFrame() const
	{
		return type == HTTP2_HEADER_FRAME;
	}
	bool isEndHead() const
	{
		return (flag & HTTP2_END_HEADER) > 0;
	}
	bool isEndStream() const
	{
		return (flag & HTTP2_END_STREAM) > 0;
	}
	bool hasPadding() const
	{
		return (flag & HTTP2_HAS_PADDING) > 0;
	}
	bool hasPriority() const
	{
		return (flag & HTTP2_HAS_PRIORITY) > 0;
	}
	SmartBuffer getData() const
	{
		return data;
	}
	const HttpHeadNode& getHead() const
	{
		return head;
	}
};

//////////////////////////////////////////////////////////
#endif