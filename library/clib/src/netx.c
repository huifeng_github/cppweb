#ifdef XG_LINUX

#include <netdb.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/statfs.h>
#include <sys/socket.h>
#include <sys/sysinfo.h>

#define ioctlsocket ioctl

#endif

#include "../netx.h"

#ifdef _MSC_VER
#pragma comment(lib, "WS2_32.lib")
#endif

BOOL IsSocketTimeout()
{
#ifdef XG_LINUX
	return errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR;
#else
	return WSAGetLastError() == WSAETIMEDOUT;
#endif
}

void SocketClose(SOCKET sock)
{
	if (IsSocketClosed(sock)) return;

#ifdef XG_LINUX
	close(sock);
#else
	closesocket(sock);
#endif
}

BOOL IsSocketClosed(SOCKET sock)
{
	return sock == INVALID_SOCKET || sock < 0;
}

int GetLocalAddress(char* host[])
{
	char hostname[256];

	if (gethostname(hostname, sizeof(hostname)) < 0) return XG_SYSERR;

	int num = 0;
	struct hostent* data = gethostbyname(hostname);

	if (host == NULL) return XG_SYSERR;

	while (num < 32 && data->h_addr_list[num])
	{
		host[num] = inet_ntoa(*(struct in_addr*)(data->h_addr_list[num]));

		num++;
	}

	return num;
}

BOOL IsLocalHost(const char* host)
{
	int cnt;
	char* vec[32];

	if (strcmp(host, LOCAL_IP) == 0) return TRUE;

	cnt = GetLocalAddress(vec);

	while (--cnt >= 0)
	{
		if (strcmp(host, vec[cnt]) == 0) return TRUE;
	}

	return FALSE;
}

SOCKET SocketConnect(const char* ip, int port)
{
	struct sockaddr_in addr;
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);

	if (IsSocketClosed(sock)) return INVALID_SOCKET;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(ip);

	if (connect(sock, (struct sockaddr*)(&addr), sizeof(addr)) == 0)
	{
		SocketSetSendTimeout(sock, SOCKECT_SENDTIMEOUT);
		SocketSetRecvTimeout(sock, SOCKECT_RECVTIMEOUT);

		return sock;
	}

	SocketClose(sock);

	return INVALID_SOCKET;
}

SOCKET SocketConnectTimeout(const char* ip, int port, int timeout)
{
	u_long mode = 1;
	struct sockaddr_in addr;
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);

	if (IsSocketClosed(sock)) return INVALID_SOCKET;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(ip);

	ioctlsocket(sock, FIONBIO, &mode); mode = 0;

	if (connect(sock, (struct sockaddr*)(&addr), sizeof(addr)) == 0)
	{
		SocketSetSendTimeout(sock, SOCKECT_SENDTIMEOUT);
		SocketSetRecvTimeout(sock, SOCKECT_RECVTIMEOUT);
		ioctlsocket(sock, FIONBIO, &mode);

		return sock;
	}

#ifdef XG_LINUX
	struct epoll_event ev;
	struct epoll_event evs;
	HANDLE handle = epoll_create(1);
	
	if (handle < 0)
	{
		SocketClose(sock);
	
		return INVALID_SOCKET;
	}
	
	memset(&ev, 0, sizeof(ev));
	
	ev.events = EPOLLOUT | EPOLLERR | EPOLLHUP;
	
	epoll_ctl(handle, EPOLL_CTL_ADD, sock, &ev);
	
	if (epoll_wait(handle, &evs, 1, timeout) > 0)
	{
		if (evs.events & EPOLLOUT)
		{
			int res = XG_ERROR;
			socklen_t len = sizeof(res);
	
			getsockopt(sock, SOL_SOCKET, SO_ERROR, (char*)(&res), &len);
			ioctlsocket(sock, FIONBIO, &mode);
	
			if (res == 0)
			{
				SocketSetSendTimeout(sock, SOCKECT_SENDTIMEOUT);
				SocketSetRecvTimeout(sock, SOCKECT_RECVTIMEOUT);
				Close(handle);
	
				return sock;
			}
		}
	}

	Close(handle);
#else
	struct timeval tv;

	fd_set ws;
	FD_ZERO(&ws);
	FD_SET(sock, &ws);

	tv.tv_sec = timeout / 1000;
	tv.tv_usec = timeout % 1000 * 1000;

	if (select(sock + 1, NULL, &ws, NULL, &tv) > 0)
	{
		int res = XG_ERROR;
		int len = sizeof(res);
	
		getsockopt(sock, SOL_SOCKET, SO_ERROR, (char*)(&res), &len);
		ioctlsocket(sock, FIONBIO, &mode);
	
		if (res == 0)
		{
			SocketSetSendTimeout(sock, SOCKECT_SENDTIMEOUT);
			SocketSetRecvTimeout(sock, SOCKECT_RECVTIMEOUT);

			return sock;
		}
	}
#endif

	SocketClose(sock);
	
	return INVALID_SOCKET;
}

BOOL GetSocketAddress(SOCKET sock, char* address)
{
	struct sockaddr_in addr;
	socklen_t len = sizeof(addr);

	if (getpeername(sock, (struct sockaddr*)(&addr), &len) == 0)
	{
#ifdef XG_LINUX
		char buffer[32] = {0};
		const char* host = inet_ntop(AF_INET, &addr.sin_addr, buffer, sizeof(buffer));
#else
		const char* host = inet_ntoa(addr.sin_addr);
#endif
		if (host)
		{
			snprintf(address, 24, "%s:%d", host, (int)(ntohs(addr.sin_port)));

			return TRUE;
		}
	}

	strcpy(address, "UNKNOWN ADDRESS");

	return FALSE;
}

SOCKET ServerSocketAccept(SOCKET svr, char* address)
{
	SOCKET sock = INVALID_SOCKET;

	if (address)
	{
		struct sockaddr_in addr;
		socklen_t len = sizeof(addr);

		sock = accept(svr, (struct sockaddr*)(&addr), &len);

		if (IsSocketClosed(sock)) return INVALID_SOCKET;

#ifdef XG_LINUX
		char buffer[32] = {0};
		const char* host = inet_ntop(AF_INET, &addr.sin_addr, buffer, sizeof(buffer));
#else
		const char* host = inet_ntoa(addr.sin_addr);
#endif
		if (host)
		{
			snprintf(address, 24, "%s:%d", host, (int)(ntohs(addr.sin_port)));
		}
		else
		{
			strcpy(address, "UNKNOWN ADDRESS");
		}
	}
	else
	{
		sock = accept(svr, NULL, NULL);

		if (IsSocketClosed(sock)) return INVALID_SOCKET;
	}

	return sock;
}

const char* GetHostAddress(const char* host, char* ip)
{
#ifdef XG_LINUX
	struct addrinfo tmp;
	struct addrinfo* res;
	struct sockaddr_in* addr;

	memset(&tmp, 0, sizeof(tmp));

	tmp.ai_family = AF_INET;
	tmp.ai_flags = AI_PASSIVE;
	tmp.ai_socktype = SOCK_STREAM;

	if (getaddrinfo(host, NULL, &tmp, &res) < 0) return NULL;

	addr = (struct sockaddr_in*)(res->ai_addr);
	ip = (char*)(inet_ntop(AF_INET, &addr->sin_addr, ip, 24));

	freeaddrinfo(res);
#else
	struct hostent* entry = gethostbyname(host);

	if (entry == NULL) return NULL;

	sprintf(ip, "%d.%d.%d.%d",
		(entry->h_addr_list[0][0] & 0x00ff),
		(entry->h_addr_list[0][1] & 0x00ff),
		(entry->h_addr_list[0][2] & 0x00ff),
		(entry->h_addr_list[0][3] & 0x00ff));
#endif
	return ip;
}

SOCKET CreateServerSocket(const char* ip, int port, int backlog)
{
	int val = 1;
	struct sockaddr_in addr;
	SOCKET sock = INVALID_SOCKET;

	sock = socket(AF_INET, SOCK_STREAM, 0);

	if (IsSocketClosed(sock)) return INVALID_SOCKET;

	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)(&val), sizeof(val));

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(ip);

	if (bind(sock, (const struct sockaddr*)(&addr), sizeof(addr)) == 0 && listen(sock, backlog) == 0) return sock;

	SocketClose(sock);

	return INVALID_SOCKET;
}

int SocketPeek(SOCKET sock, void* data, int size)
{
	int res = recv(sock, data, size, MSG_PEEK);

	if (res > 0) return res;

	if (res == 0) return XG_NETCLOSE;

	if (IsSocketTimeout()) return 0;

	return XG_NETERR;
}

int SocketRead(SOCKET sock, void* data, int size)
{
	return SocketReadEx(sock, data, size, TRUE);
}

int SocketReadEx(SOCKET sock, void* data, int size, BOOL completed)
{
	char* str = (char*)(data);

	if (completed)
	{
		int res = 0;
		int times = 0;
		int readed = 0;

		while (readed < size)
		{
			res = recv(sock, str + readed, size - readed, 0);

			if (res > 0)
			{
				if (res > SOCKET_TIMEOUT_LIMITSIZE)
				{
					times = 0;
				}
				else
				{
					if (++times > SOCKET_TIMEOUT_REDOTIMES) return XG_TIMEOUT;
				}

				readed += res;
			}
			else if (res == 0)
			{
				return XG_NETCLOSE;
			}
			else
			{
				if (IsSocketTimeout())
				{
					if (++times > SOCKET_TIMEOUT_REDOTIMES) return XG_TIMEOUT;
				}
				else
				{
					return XG_NETERR;
				}
			}
		}

		return readed;
	}
	else
	{
		int res = recv(sock, str, size, 0);

		if (res > 0) return res;

		if (res == 0) return XG_NETCLOSE;

		if (IsSocketTimeout()) return 0;

		return XG_NETERR;
	}
}

int SocketWrite(SOCKET sock, const void* data, int size)
{
	return SocketWriteEx(sock, data, size, TRUE);
}

int SocketWriteEx(SOCKET sock, const void* data, int size, BOOL completed)
{
	const char* str = (const char*)(data);

	if (completed)
	{
		int res = 0;
		int times = 0;
		int writed = 0;

		while (writed < size)
		{
			res = send(sock, str + writed, size - writed, 0);

			if (res > 0)
			{
				if (res > SOCKET_TIMEOUT_LIMITSIZE)
				{
					times = 0;
				}
				else
				{
					if (++times > SOCKET_TIMEOUT_REDOTIMES) return XG_TIMEOUT;
				}
	
				writed += res;
			}
			else
			{
				if (IsSocketTimeout())
				{
					if (++times > SOCKET_TIMEOUT_REDOTIMES) return XG_TIMEOUT;
				}
				else
				{
					return XG_NETERR;
				}
			}
		}
		
		return writed;
	}
	else
	{
		int res = send(sock, str, size, 0);

		if (res > 0) return res;

		if (IsSocketTimeout()) return 0;

		return XG_NETERR;
	}
}

int SocketReadLine(SOCKET sock, char* msg, int len)
{
	int val = 0;
	int readed = 0;

	while (readed < len)
	{
		if ((val = SocketReadEx(sock, msg + readed, len - readed, FALSE)) < 0) return val;

		if (val == 0 || (readed += val) < 2) continue;

		if (msg[readed - 2] == '\r' && msg[readed - 1] == '\n')
		{
			msg[readed - 2] = 0;

			return readed - 2;
		}
	}

	return XG_DATAERR;
}

int SocketWriteEmptyLine(SOCKET sock)
{
	return SocketWrite(sock, "\r\n", 2);
}
int SocketWriteLine(SOCKET sock, const char* msg, int len)
{
	if (len + 2 > 64 * 1024)
	{
		len = SocketWrite(sock, msg, len);

		return (len > 0) ? SocketWrite(sock, "\r\n", 2) : len;
	}
	else
	{
		char buffer[64 * 1024];

		memcpy(buffer, msg, len);
		buffer[len++] = '\r';
		buffer[len++] = '\n';

		return SocketWrite(sock, buffer, len);
	}
}

BOOL SocketSetSendTimeout(SOCKET sock, int ms)
{
#ifdef XG_LINUX
	struct timeval tv;

	tv.tv_sec = ms / 1000;
	tv.tv_usec = ms % 1000 * 1000;

	return setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (char*)(&tv), sizeof(tv)) == 0;
#else
	return setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, (char*)(&ms), sizeof(ms)) == 0;
#endif
}

BOOL SocketSetRecvTimeout(SOCKET sock, int ms)
{
#ifdef XG_LINUX
	struct timeval tv;

	tv.tv_sec = ms / 1000;
	tv.tv_usec = ms % 1000 * 1000;

	return setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)(&tv), sizeof(tv)) == 0;
#else
	return setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)(&ms), sizeof(ms)) == 0;
#endif
}