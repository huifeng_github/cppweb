#ifndef XG_CMD_H
#define XG_CMD_H
/////////////////////////////////////////////////////
#include "File.h"

namespace cmdx
{
	void puts(const string& msg);
	void printf(const char* fmt, ...);
	bool CheckCommand(const char* fmt, ...);
	void PrintHexData(const void* msg, int len);
	int SelectCommand(const vector<string>& menus, const char* msg = NULL);
	int RunCommand(const string& cmd, char* buffer = NULL, int size = 0, bool hidden = true);
};

namespace proc
{
	int argc();
	const char* argv(int index);
	const char* argv(const char* key);
	Process* init(int argc, char** argv);

	string pwd();
	string pwd(const string& path);

	string home();
	string exepath();
	string exename();
	string appname();
	string env(const string& key);
	string env(const string& key, const string& val);

	long tid();
	void daemon();
	PROCESS_T pid();
	void exit(int code = 0);
	void wait(PROCESS_T pid);
	void kill(PROCESS_T pid, int flag = 0);

	vector<ProcessData> ps();
	vector<ProcessData> ps(string name);

	template<class DATA_TYPE>
	string quote(const DATA_TYPE& val)
	{
		return stdx::quote(val);
	}
	template<class DATA_TYPE, class ...ARGS>
	string quote(const DATA_TYPE& val, ARGS ...args)
	{
		return stdx::quote(val) + " " + quote(args...);
	}
	template<class ...ARGS>
	SmartBuffer exec(const string& val, ARGS ...args)
	{
		SmartBuffer data(1024 * 1024);

		int len = cmdx::RunCommand(quote(val, args...), data.str(), data.size(), true);
		
		if (len > 0)
		{
			data.truncate(len);
		}
		else
		{
			data.free();
		}

		return data;
	}
};

namespace path
{
	string name(const string& path);
	string parent(const string& path);
	string extname(const string& path);

	string cat(const string& path);
	bool copy(const string& src, const string& dest);
	bool rename(const string& src, const string& dest);
	sp<XFile> create(const string& path, bool clear = true);
	sp<XFile> open(const string& path, E_OPEN_MODE mode = eWRITE);

	int type(const string& path);
	long size(const string& path);
	bool mkdir(const string& path);
	bool isdir(const string& path);
	bool isfile(const string& path);
	bool exists(const string& path);
	bool remove(const string& path);
	time_t mtime(const string& path);
	time_t mtime(const string& path, const time_t& tm);
	vector<string> dir(const string& path, bool file = true);
	vector<string> find(const string& path, const string& filter);
};
/////////////////////////////////////////////////////
#endif