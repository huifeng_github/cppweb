#ifndef XG_SEMAPHORE_H
#define XG_SEMAPHORE_H
/////////////////////////////////////////////////////////////
#include "std.h"

class Semaphore : public Object
{
protected:
	bool created;
	HANDLE handle;

public:
	Semaphore();
	~Semaphore();
	void close();
	bool wait() const;
	bool canUse() const;
	bool release() const;
	bool open(HANDLE handle);
	bool open(const string& name);
	bool create(const string& name, int size = 1);
};
/////////////////////////////////////////////////////////////
#endif